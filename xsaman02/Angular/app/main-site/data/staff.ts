export interface Staff {
    login : String;
    value : Number;
}

export interface StaffName {
    login : String;
    name : String;
}

export const staff : Staff[] = [
    {login : "xhavle01", value : 0},
    {login : "xnovak66", value : 1},
    {login : "xruzic11", value : 2},
    {login : "xrezac00", value : 3},
    {login : "xstoja07", value : 4}
];

export const staffNames : StaffName[] = [
    {login : "xhavle00", name : "Pavel Havlena"},
    {login : "xnevim00", name : "Já Nevím"},
    {login : "xsaman02", name : "Jan Šamánek"},
    {login : "xstoja07", name : "Radomír Stojan"},
    {login : "xjezek01", name : "Jan Ježek"},
    {login : "xkolar00", name : "Dušan Kolář"},
    {login : "xperin00", name : "Dr. Pepe"},
    {login : "xnovak00", name : "Debil Novák"},
    {login : "xexamp00", name : "Head Example"},
    {login : "ruzic11", name : "Pavel Růžička"}
]
