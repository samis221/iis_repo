export interface PendingCourses {
    code : String
    term : Date
    approved : String
}


export const pendingCourses : PendingCourses[] = [
    {code: "IPK", term: new Date(), approved: "no"},
    {code: "IZP", term: new Date(), approved: "no"},
    {code: "IKR", term: new Date(), approved: "no"},
    {code: "ITY", term: new Date(), approved: "no"},
    {code: "IFJ", term: new Date(), approved: "no"}
]