declare var hours: Number;
declare var minutes: Number;
declare var day : Number;
declare var month : Number;


function setStartDate() {

    let date = new Date();

    let sign = Math.random() < 0.5 ? -1 : 1;
    let dayrange = Math.floor(Math.random() * 10 * sign);
    let hours = Math.floor(Math.random() * 17);
    if (hours < 8) {
        hours = 7 + hours;
    }

    date.setDate(date.getDate() + dayrange);
    date.setHours(hours);
    date.setMinutes(Math.floor(Math.random() * 59));

    globalThis.month = date.getMonth();
    globalThis.day = date.getDate();
    globalThis.hours = date.getHours();
    globalThis.minutes = date.getMinutes();

    return date;
}

function setEndDate() {

    let date = new Date();
    date.setHours(globalThis.hours + 2);
    date.setMinutes(globalThis.minutes);
    date.setDate(globalThis.day);
    date.setMonth(globalThis.month);

    return date;
}

export class Events {
    text: String;
    startDate: Date;
    endDate: Date;
}

export const data: Events[] = [{
    text: "IZP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, 
{
    text: "IMP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, 
{
    text: "INM",
    startDate: setStartDate(),
    endDate: setEndDate()
},
{
    text: "IMS",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IZP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IZP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IMP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IPP",
    startDate: setStartDate(),
    endDate: setEndDate()
},
{
    text: "IPP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IMS",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "INM",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "IMP",
    startDate: setStartDate(),
    endDate: setEndDate()
}, {
    text: "ISS",
    startDate: setStartDate(),
    endDate: setEndDate()
}
];
