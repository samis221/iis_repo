export class Address {
    country : String
    address : String
    postalCode : String
}

export class AccountsDetail {
    login : string
    firstName : String
    lastName : String
    titlesBefore : String
    titlesAfter : String
    numbers : {number : String}[]
    role : String
    address : Address
}

export const accountsDetail : AccountsDetail[] = [
    {login : "xsaman02", role : "Student", firstName : "Jan", lastName : "Šamánek", titlesBefore : "bc.", titlesAfter : "", numbers : [{number : "776795556"},{number : "332455982"}], 
        address : {country : "Czech republic", address : "SNP 1190", postalCode : "76502"}},
    {login : "xstoja07", role : "Student", firstName : "Radomír", lastName : "Stojan", titlesBefore : "ing.", titlesAfter : "csc.", numbers : [{number : "533894556"},{number : "232849023"},{number : "993838575"}], 
        address : {country : "Czech republic", address : "nvw Kvítkovice 42", postalCode : "76503"}},
    {login : "xludvi08", role : "Leader", firstName : "Ludvík", lastName : "Ludvík", titlesBefore : "RNDr. ing.", titlesAfter : "", numbers : [{number : "605983444"}], 
        address : {country : "Germany", address : "Dresden Faushwein 3340", postalCode : "92233"}},
    {login : "xnevim00", role : "Head", firstName : "Richard", lastName : "nevim", titlesBefore : "", titlesAfter : "", numbers : [], 
        address : {country : "Hungary", address : "Budapest 1122", postalCode : "11111"}},
]
