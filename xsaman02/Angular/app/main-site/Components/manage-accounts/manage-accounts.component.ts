import { Component, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MainSiteService } from '../../services/main-site.service';
import DataGrid from "devextreme/ui/data_grid";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.css']
})
export class ManageAccountsComponent implements OnInit, AfterContentInit, OnDestroy {

  login : string;
  role : string;
  selectedRowsLength : number
  dataGridInstance: DataGrid;
  accountDetails = new Array<{address : "", 
                              firstName : "", 
                              lastName : "", 
                              login : "", 
                              role : "", 
                              numbers : "", 
                              titlesBefore : "", 
                              titlesAfter : ""}>();
  accountList;
  accountForms : FormGroup[];
  isLoading = true;
  edit = false;
  onDestroy$: Subject<Boolean> = new Subject();
  
  constructor(private cookie : CookieService, private servise : MainSiteService,
               private fb : FormBuilder, private router : Router,
               private location : Location, private _snackBar: MatSnackBar) 
  {
    if(this.cookie.get('role') != 'admin')
    {
      this.location.back();
    }
  }

  async ngOnInit() {
    await this.getAccountList()
    
    this.accountForms = new Array<FormGroup>();

    this.login = this.cookie.get("login");
    this.role = this.cookie.get("role");
    this.isLoading = false;
  }

  ngAfterContentInit()
  {
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  saveGridInstance (e) {
    this.dataGridInstance = e.component;
  }

  async EditMarked()
  {
    this.isLoading = true;
    this.accountDetails = [];
    this.accountForms = [];
    let rows = this.selectedRows;

    if (this.edit == true || rows.length == 0) 
    {
      this.edit = false;
      this.isLoading = false;
      return;
    }

    let delayTime = 50;
    this.selectedRowsLength = rows.length;

    rows.forEach(async l => {
      await this.delay(delayTime);
      await this.getDetail(l.login)
    });
    
    await this.delay(delayTime * this.selectedRowsLength + 500);

    // let i = 0;
    this.accountDetails.forEach(d => 
    {
      this.accountForms.push(this.CreateForm(d));
    });    

    this.edit = !this.edit;
    this.isLoading = false;
  }

  async getDetail(login)
  {
    let detail;
    await this.servise.getAccountDetail(login).then((data) => {detail = data});    
    this.accountDetails.push(detail)
  }

  async getAccountList()
  {
    this.accountList = await this.servise.getAccountList();
  }

  CreateForm(detail)
  {
    return this.fb.group({
      login : this.fb.control(detail.login, Validators.required),
      firstName : this.fb.control(detail.firstName, [Validators.required, Validators.maxLength(100)]),
      lastName : this.fb.control(detail.lastName, [Validators.required, Validators.maxLength(100)]),
      titlesBefore : this.fb.control(detail.titlesBefore, Validators.maxLength(100)),
      titlesAfter : this.fb.control(detail.titlesAfter, Validators.maxLength(100)),
      number : this.fb.control(detail.numbers,Validators.maxLength(13)),
      email : this.fb.control(detail.address, Validators.email)
    });
  }

  PopulateForm(i)
  {
    this.accountForms[i].patchValue(this.accountDetails[i]);
  }

  async DeleteMarked()
  {
    this.isLoading = true;
    let rows = this.selectedRows;
    rows.forEach(async a => {
      if (a.role != "Student") {
        this.openSnackBar("Could not delete " + a.login + " because of database constrains", "Okay.");
      }
      else
      {
        await this.delay(50);
        let res = await this.servise.deleteAccount(a.login);
        if (res == undefined) {
          this.openSnackBar("Did not manage to delete " + a.login, "Okay");
        }
        else
        {
          this.openSnackBar("Deleted account " + a.login, "Horaay!");
        }
      }
    });

    await this.delay(100);
    await this.getAccountList();    
    await this.delay(100);
    this.dataGridInstance.refresh();
    this.isLoading = false;
  }

  async saveModified()
  {
    let invalid = 0 ;
    this.accountForms.forEach(async form => {
      await this.delay(200)
      let res = await this.servise.modifyAccount(form.value);
      
      if (res == undefined) {
        invalid++;
        console.error(form.value.login + " cannot be modified");
      }
    });

    if (invalid == 0) 
    {
      this.openSnackBar("All transactions were succesfully done", "Horaay!");
      this.router.navigate(["Schedule"]);
    }
    else
      this.openSnackBar(invalid + " transactions was invalid", "Okay");

    this.getAccountList();
    this.delay(500);
    this.dataGridInstance.refresh();
  }

  isValid(form : FormGroup)
  {
    return form.valid;
  }

  isAllValid()
  {
    let valid = true;
    this.accountForms.forEach(form => {
      if (form.invalid)
        valid = false;
    });

    return valid; 
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  get selectedRows()
  {
    return this.dataGridInstance.getSelectedRowsData();
  }

  // get isValid()
  // {
  //   console.log(this.accountForms.every( f => { f.valid }));
  //   return this.accountForms.every( f => { f.valid });
  // }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
