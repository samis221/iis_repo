import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MainSiteService } from '../../services/main-site.service';
import { Location } from '@angular/common';
import DataGrid from "devextreme/ui/data_grid";
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-approve-registrations',
  templateUrl: './approve-registrations.component.html',
  styleUrls: ['./approve-registrations.component.css']
})
export class ApproveRegistrationsComponent implements OnInit, OnDestroy {

  dataGridInstance : DataGrid;
  registrations;
  onDestroy$: Subject<Boolean> = new Subject();
  login : string;
  isLoading = true;

  constructor(private cookie : CookieService, private router : Router,
    private service : MainSiteService, private _snackBar : MatSnackBar) { }


  async ngOnInit() {
    let role = this.cookie.get('role');
    this.login = this.cookie.get('login');
    if(role != 'guarantor' && role != 'head' && role != 'admin')
    {
      alert("Something went wrong. Need to resign.")
      this.router.navigate(["Schedule"]);
    }
    else
    {
      this.registrations = await this.service.getPendingRegistrations();     
    }
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }


  saveGridInstance (e) {
    this.dataGridInstance = e.component;
  }

  async Save()
  {
    this.isLoading = true;
    let invalid = 0;
    this.registrations.forEach(async r => {
      if (r.approved != "Pending") {
        let res = await this.service.modifyPendingRegistrations(r, this.login);
        await this.delay(50);
        if (res == undefined) 
        {
          console.log("Could not modify " + r.login + " on " + r.course);
          invalid++;
        }
        else
        {
          console.log("modified " + r.login + " on " + r.course);
        }
      }
    });
    if (invalid == 0) 
    {
      this.openSnackBar("All transactions were succesfull", "Horaay!");
    }
    else
    {
      this.openSnackBar(invalid + " transactions were invalid", "Okay");
    }

    await this.delay(300);
    this.registrations = await this.service.getPendingRegistrations();
    this.RefreshTable();
    this.isLoading = false;
  }

  ApproveAll()
  {
    this.registrations.forEach(e => {
      e.approved = "Yes";
    });
    this.RefreshTable();
  }

  ApproveMarked()
  {
    let rows = this.dataGridInstance.getSelectedRowsData();
    for (let i = 0; i < rows.length; i++) {
      for (let x = 0; x < this.registrations.length; x++) {
        if (rows[i].login == this.registrations[x].login && 
            rows[i].course == this.registrations[x].course &&
            rows[i].term == this.registrations[x].term) {
          
          this.registrations[x].approved = "Yes";          
          break;
        }        
      }      
    }

    this.RefreshTable();
  }

  ForbidAll()
  {
    this.registrations.forEach(e => {
      e.approved = "No";
    });
    this.RefreshTable();
  }

  ForbidMarked()
  {
    let rows = this.dataGridInstance.getSelectedRowsData();
    for (let i = 0; i < rows.length; i++) {
      for (let x = 0; x < this.registrations.length; x++) {
        if (rows[i].login == this.registrations[x].login && 
          rows[i].course == this.registrations[x].course &&
          rows[i].term == this.registrations[x].term) {
          
          this.registrations[x].approved = "No";
          break;
        }        
      }      
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }


  private RefreshTable()
  {
    this.dataGridInstance.refresh();
  }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
