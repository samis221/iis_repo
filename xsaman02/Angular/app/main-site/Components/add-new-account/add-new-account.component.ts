import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { MainSiteService } from '../../services/main-site.service';
import { Observable, pipe } from "rxjs";
import { map } from "rxjs/operators";
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { logging } from 'protractor';

@Component({
  selector: 'app-add-new-account',
  templateUrl: './add-new-account.component.html',
  styleUrls: ['./add-new-account.component.css']
})
export class AddNewAccountComponent implements OnInit {

  newForm : FormGroup;
  roleOptions;
  countryOptions;
  login;
  signRole;

  // confirmedPassword;
  constructor(private fb : FormBuilder, private service : MainSiteService,
    private router : Router, private _snackBar: MatSnackBar,
    private activeRoute : ActivatedRoute) { }

  ngOnInit() {
    this.login = this.activeRoute.snapshot.queryParams["login"];
    this.signRole = this.activeRoute.snapshot.queryParams["role"];

    this.CreateForm();
    this.roleOptions = this.service.getRoles();
    this.countryOptions = this.service.getCountries();
  }

  CreateForm()
  {
    const specialization = this.fb.group({
      yearOfStudy : this.fb.control(1), //student
      institute : this.fb.control(''),   //Head
    });

    this.newForm = this.fb.group({
      firstName : this.fb.control('', [Validators.required]),
      lastName : this.fb.control('', [Validators.required]),
      email : this.fb.control('',Validators.email),
      number : this.fb.control(''),
      titlesBefore : this.fb.control(''),
      titlesAfter : this.fb.control(''),
      role : this.fb.control('', Validators.required),
      password : this.fb.control('', [Validators.required, Validators.minLength(8)]),
      confirmPassword : this.fb.control('', Validators.required),
      specialization : specialization
    })
  }

  async onSubmit()
  {
    await this.delay(100);
    let res = await this.service.addNewAccount(this.newForm);

    if (res == undefined) {
      this.openSnackBar("Bad request... Already exists", "Okay");
    }
    else{
      this.openSnackBar("Succesfully created account", "Hooray!");
      this.router.navigate(["/Schedule"], {queryParams : {login : this.login, role : this.signRole}});
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  get role()
  {
    return this.newForm.get("role");
  }

  get roles()
  {
    return this.roleOptions;
  }

  get countries()
  {
    return this.countryOptions;
  }

  get firstName()
  {
    return this.newForm.get("firstName");
  }

  get password()
  {
    return this.newForm.value.password;
  }


  get confirmPassword()
  {
    return this.newForm.value.confirmPassword;
  }

  get lastName()
  {
    return this.newForm.get("lastName");
  }

  get phones()
  {
    return this.newForm.get('number');
  }

  get institute()
  {
    return this.newForm.value.specialization.value.institute
  }

  get titlesBefore()
  {
    return this.newForm.get('titlesBefore');
  }

  get titlesAfter()
  {
    return this.newForm.get('titlesAfter');
  }

  private delay(ms: number) : Promise<void> {
    return new Promise<void>(resolve =>
      setTimeout(resolve, ms));
  }

}
