import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MainSiteService } from '../../services/main-site.service';
import { SignUpService } from 'src/app/sign-up.service';
import { Location, UpperCasePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.css']
})
export class NewCourseComponent implements OnInit, AfterContentInit, OnDestroy {

  newCourseForm : FormGroup;
  login : String;
  private role = null;
  isLoading = true;
  staff = null;
  rooms = null;
  guarantors = null;
  onDestroy$: Subject<Boolean> = new Subject();

  dxTerms = [
    {name : '', lecturer : '', startDate : new Date(), endDate : new Date(), room : '', maxScore : 0}
  ]

  constructor(private fb : FormBuilder, private service : MainSiteService,
    private router : Router, private _snackBar: MatSnackBar,
    private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
      this.login = this.activatedRoute.snapshot.queryParams["login"];
      this.CreateForm()

      this.getStaff();
      this.getRooms();
      this.getGuarantors();

      this.isLoading = false;
      
      // this.staff = this.service.getStaff()
      //   .pipe(takeUntil(this.onDestroy$))
      //   .subscribe((data) => {this.staff = data;
      //   }, (error) => {});
      
      // this.rooms = this.service.getRooms()
      //   .pipe(takeUntil(this.onDestroy$))
      //   .subscribe((data) => {this.rooms = data;}, (error) => {});
  }

  async getStaff()
  {
    await this.delay(500);
    this.staff = await this.service.getStaff();
  }

  async getRooms()
  {
    await this.delay(500);
    this.rooms = await this.service.getRooms();
  }

  async getGuarantors()
  {
    await this.delay(500);
    this.guarantors = await this.service.getGuarantors();
  }

  ngAfterContentInit()
  {
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  CreateForm()
  {
    this.newCourseForm = this.fb.group({
      code : this.fb.control('', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]),
      name : this.fb.control('', [Validators.required, Validators.maxLength(100)]),
      description : this.fb.control('', Validators.maxLength(500)),
      head : this.fb.control(this.login),
      guarantor : this.fb.control(this.login, Validators.required),
      terms : this.fb.array([])
    })
  }

  AddTerm(dxTerm)
  {
    if (!dxTerm.lecturer || !dxTerm.name || !dxTerm.startDate || !dxTerm.endDate || !dxTerm.room || typeof(dxTerm.maxScore) != typeof(0)) 
    {
      
      return false;
    }
    const term = this.fb.group({
      name : this.fb.control(dxTerm.name, Validators.maxLength(100)),
      lecturer : this.fb.control(dxTerm.lecturer),
      startDate : this.fb.control(dxTerm.startDate),
      endDate : this.fb.control(dxTerm.endDate),
      maxScore : this.fb.control(dxTerm.maxScore, [Validators.min(0), Validators.max(100)]),
      room : this.fb.control(dxTerm.room)
    });

    this.terms.push(term);
    return true;
  }

  async Save()
  {
    this.terms.clear();
    if (this.newCourseForm.invalid) {
      this.openSnackBar("Something in form is invalid. Check length of max score and other", "Okay");
      return;
    }
    this.isLoading = true;
    for (let i = 0; i < this.dxTerms.length; i++) {
      if(! this.AddTerm(this.dxTerms[i]))
      {
        this.openSnackBar("One or more terms are not filled. Course will not be created", "Okay");
        this.isLoading = false;
        return;
      }
    }

    this.newCourseForm.value.code = new UpperCasePipe().transform(this.newCourseForm.value.code);
    await this.delay(100);
    let res = await this.service.addNewCourse(this.newCourseForm);
    if (res == undefined) {
      this.openSnackBar("Bad request... Already exists", "Okay");
      this.isLoading = false;
    }
    else
    {
      this.openSnackBar("Succesfully created course " + this.newCourseForm.value.code, "Hooray!");
      this.router.navigate(["/Schedule"], {queryParams : {login : this.login, role : this.role}});
    }    
  }

  DeleteTerm(i)
  {
    this.terms.removeAt(i);
  }

  onValueChanged(e, i) {  
    if (this.dxTerms[i])  
      this.dxTerms[i].lecturer = e.value;  
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  get terms()
  {
    return this.newCourseForm.get("terms") as FormArray;
  }

  get code()
  {
    return this.newCourseForm.get("code");
  }

  get name()
  {
    return this.newCourseForm.get("name");
  }

  private delay(ms: number) : Promise<void> {
    return new Promise<void>(resolve =>
      setTimeout(resolve, ms));
  }
}
