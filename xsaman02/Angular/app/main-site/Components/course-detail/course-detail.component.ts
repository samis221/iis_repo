import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainSiteService } from '../../services/main-site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { takeUntil} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Location, UpperCasePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

interface Term {
  id : number
  startDate : Date
  endDate : Date
  lector : String
}

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit, OnDestroy {


  code;
  detail;
  copyDetail;
  role : string;
  login : string;
  edit = false;
  form: FormGroup;
  onDestroy$: Subject<Boolean> = new Subject();
  isLoading = true;

  leaders;
  heads;
  educators;
  rooms;

  headName;
  leaderName;

  constructor(private service: MainSiteService, private cookie: CookieService,
    private fb: FormBuilder, private route : ActivatedRoute,
    private _snackBar : MatSnackBar,private router : Router) { }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  async ngOnInit() {
    this.code = this.route.snapshot.paramMap.get("code");
    this.login = this.cookie.get("login");

    this.detail = this.service.getCourseDetail(this.code)
                  .pipe(takeUntil(this.onDestroy$))
                  .subscribe((data) => {this.detail = data;
                  }, (error) => {console.error(error);
                  });

    await this.getLeaders();
    await this.getHeads();
    await this.getEducators();
    await this.getRooms();

    this.copyDetail = this.detail;
    this.getHeadNameFor(this.detail.head);
    this.getLeaderNameFor(this.detail.leader);
    
    this.isLoading = false;
  }

  HasRight() {
    this.role = this.route.snapshot.queryParamMap.get("role");
    return this.role == "head" || this.role == "admin" || this.role == "guarantor";
  }

  async getHeads()
  {
    this.heads = await this.service.getHeads();
  }

  async getLeaders()
  {
    this.leaders = await this.service.getGuarantors();
  }

  async getEducators()
  {
    this.educators = await this.service.getStaff();
  }
  
  async getRooms()
  {
    this.rooms = await this.service.getRooms();
  }

  async getNameByLogin(login)
  {
    this.service.getNameByLogin(login);
  }



  EditCourse() {
    if (this.edit == true) {
      this.edit = false;
      return
    }
    this.CreateForm()
    this.edit = !this.edit;
  }


  CreateForm() {
    this.form = this.fb.group({
      code: this.fb.control(this.detail.code, [Validators.required, Validators.minLength(3), Validators.maxLength(3)]),
      name: this.fb.control(this.detail.name, [Validators.required, Validators.maxLength(100)]),
      head: this.fb.control(this.detail.head, Validators.required),
      guarantor: this.fb.control(this.detail.leader, Validators.required),
      description: this.fb.control(this.detail.description, Validators.maxLength(500))
    });
  }

  async SaveCourse()
  {

    let putForm = 
    {
      oldCode : this.copyDetail.code,
      course : 
      {
        code : new UpperCasePipe().transform(this.form.value.code),
        name : this.form.value.name,
        head : this.form.value.head,
        guarantor : this.form.value.guarantor,
        description : this.form.value.description
      },
      terms : Array<any>()
    }

    this.detail.date_of_course.forEach(term => {
      if (term.id && term.startDate && term.endDate && term.lecturer && term.room) 
      {  
        putForm.terms.push(
          {
            oldId : term.copyId,
            term : {
              id : term.id,
              startDate : term.startDate,
              endDate : term.endDate,
              lecturer : term.lecturer,
              room : term.room
            }
          }
        )
        if (putForm.terms[putForm.terms.length - 1].oldId == undefined) 
        {
          putForm.terms[putForm.terms.length - 1].oldId = term.id;
        }
      }
      else
      {
        console.error("Could not create " + term.id + " term");
      }
    });

    let res = await this.service.updateCourse(putForm);
    
    if (res) 
    {  
      this.OpenSnackBar("Update completed", "Horaay!");
      this.router.navigate(["AllCourses"]);
    }
    else
    {
      console.error("Something went wrong!");
    }
  }

  async SignCourse(term)
  {
    let httpBody = {id: term.id, capacity: term.maxCapacity, login: this.login};
    let res = await this.service.signCourse(httpBody);
    await this.delay(200);

    if (res == undefined) {
      this.OpenSnackBar("Could not sign course " + term.id + ", probably full or already signed", "Okay");
    }
    else
    {
      this.OpenSnackBar("Congratulations, you sign " + term.id, "Horaay!");
      this.router.navigate(["Schedule"]);
    }
  }

  async deleteCourse()
  {
    let code = this.copyDetail.code;
    let res = await this.service.deleteCourse(code);
    await this.delay(200);

    if (res) 
    {
      this.OpenSnackBar("Course " + code + " deleted", "Thanks!");
      this.router.navigate(["Schedule"]);
    }
    else
    {
      this.OpenSnackBar("Could not delete course " + code, "Okay");
    }
  }

  getHeadNameFor(login : string)
  {    
    this.educators.forEach(e => {
      if (e.login == login) {
        this.headName = e.name;
      }
    });

    return null;
  }

  getLeaderNameFor(login : string)
  {    
    this.educators.forEach(e => {
      if (e.login == login) {
        this.leaderName = e.name;
      }
    });

    return null;
  }

  private OpenSnackBar(message, action)
  {
    this._snackBar.open(message, action, {
      duration: 4000,
    });
  }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
