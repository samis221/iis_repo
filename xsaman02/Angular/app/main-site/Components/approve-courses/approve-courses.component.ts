import { Component, OnInit, OnDestroy } from '@angular/core';
import DataGrid from "devextreme/ui/data_grid";
import { MainSiteService } from '../../services/main-site.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-approve-courses',
  templateUrl: './approve-courses.component.html',
  styleUrls: ['./approve-courses.component.css']
})
export class ApproveCoursesComponent implements OnInit, OnDestroy {

  dataGridInstance : DataGrid;
  onDestroy$: Subject<Boolean> = new Subject();
  courses;

  constructor(private cookie : CookieService, private router : Router,
              private service : MainSiteService, private _snackBar : MatSnackBar) { }

  ngOnInit() {
    let role = this.cookie.get('role');
    if(role != 'head' && role != 'admin')
    {
      alert("Something went wrong. Need to resign");
      this.cookie.deleteAll()
      this.router.navigate(["SignUp"]);
    }
    else
    {
      this.courses = this.service.getPendingCourses()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data) => {this.courses = data;}, (error) => {});     
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  saveGridInstance (e) {
    this.dataGridInstance = e.component;
  }

  Save()
  {
    let invalid = 0;
    this.courses.forEach(async c => {
      if (c.approved != "Pending") {
        await this.delay(50);
        let res = await this.service.modifyPendingCourses(c);
        if (res == undefined) 
        {
          console.error("Could not modify " + c.name);
          invalid++;
        }
        else
        {
          console.log("modified " + c.name);
        } 
      }
    });

    if (invalid == 0) 
    {
      this.openSnackBar("All transactions done succesfully", "Horaay");
    }
    else
    {
      this.openSnackBar(invalid + " transactions were invalid", "Okay");
    }
    this.router.navigate(["Schedule"]);
  }

  ApproveAll()
  {
    this.courses.forEach(e => {
      e.approved = "Yes";
    });
    this.RefreshTable();
  }

  ApproveMarked()
  {
    let rows = this.dataGridInstance.getSelectedRowsData();
    for (let i = 0; i < rows.length; i++) {
      for (let x = 0; x < this.courses.length; x++) {
        if (rows[i].term == this.courses[x].term && rows[i].code == this.courses[x].code) {
          this.courses[x].approved = "Yes";          
          break;
        }        
      }      
    }

    this.RefreshTable();
  }

  ForbidAll()
  {
    this.courses.forEach(e => {
      e.approved = "No";
    });
    this.RefreshTable();
  }

  ForbidMarked()
  {
    let rows = this.dataGridInstance.getSelectedRowsData();
    for (let i = 0; i < rows.length; i++) {
      for (let x = 0; x < this.courses.length; x++) {
        if (rows[i].term == this.courses[x].term && rows[i].code == this.courses[x].code) {
          this.courses[x].approved = "No";
          break;
        }        
      }      
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  private RefreshTable()
  {
    this.dataGridInstance.refresh();
  }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
