import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainSiteService } from '../../services/main-site.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
// import DataGrid from "devextreme/ui/data_grid";


@Component({
  selector: 'app-all-courses-list',
  templateUrl: './all-courses-list.component.html',
  styleUrls: ['./all-courses-list.component.css']
})
export class AllCoursesListComponent implements OnInit, OnDestroy {

  coursesList;
  role : string;
  login : string;
  onDestroy$: Subject<Boolean> = new Subject();
  constructor(private service : MainSiteService, private cookie : CookieService) { }

  ngOnInit() {
    this.login = this.cookie.get("login");
    this.role = this.cookie.get("role");

    this.coursesList = this.service.getCoursesList()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data) => {this.coursesList = data;}, (error) => {});
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  HasRight()
  {    
    return this.role == "admin" || this.role == "guarantor" || this.role == "head";
  }

}
