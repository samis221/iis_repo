import { Component, OnInit } from '@angular/core';
import DataGrid from "devextreme/ui/data_grid";
import { Location } from '@angular/common';
import { MainSiteService } from '../../services/main-site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-score-term',
  templateUrl: './score-term.component.html',
  styleUrls: ['./score-term.component.css']
})
export class ScoreTermComponent implements OnInit {

  dataGridInstance : DataGrid;
  termAccounts;
  onDestroy$: Subject<Boolean> = new Subject();
  value : FormGroup;
  isLoading = true;
  maxScore = 0;
  
  constructor(private router : Router, private service : MainSiteService,
    private fb : FormBuilder, private cookie : CookieService,
    private _snackBar : MatSnackBar, private activeRoute : ActivatedRoute) { }

  async ngOnInit() {
    let role = this.cookie.get('role');
    let term = this.activeRoute.snapshot.paramMap.get('term');

    if(role == 'student' || role == 'host' || term == '' || term == null)
    {
      alert("Something went wrong. Need to resign");
      this.cookie.deleteAll();
      this.router.navigate(["SignUp"]);
    }
    else
    {

      await this.getTermStudents(term);
      await this.delay(300);

      this.maxScore = this.termAccounts.maxRating;
      this.termAccounts = this.termAccounts.students;

      
      this.value = this.fb.group({
        scoreValue : this.fb.control(0, [Validators.max(this.maxScore), Validators.min(0), Validators.required])
      })
    }
    
    this.isLoading = false;
  }

  async getTermStudents(term)
  {
    this.termAccounts = await this.service.getTermStudents(term);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  saveGridInstance (e) {
    this.dataGridInstance = e.component;
  }

  Save()
  {
    let res;
    let invalid = 0;
    this.termAccounts.forEach(async c => {
      res = undefined;
      await this.delay(50);
      res = await this.service.scoreAccountOnTerm(c);

      if (res == undefined) 
      {
        invalid++;
        console.error("Could not score " + c.login);
      }
    });

    if (invalid == 0) 
      this.OpenSnackBar("All transactions were succesfull!", "Horaay!");
    else
      this.OpenSnackBar(invalid + " transactions were invalid", "Okay");

    this.router.navigate(["Schedule"]);
  }

  SetChanges()
  {
    let rows = this.dataGridInstance.getSelectedRowsData();
    for (let i = 0; i < rows.length; i++) {
      for (let x = 0; x < this.termAccounts.length; x++) {
        if (rows[i].login == this.termAccounts[x].login) {
          this.termAccounts[x].rating = this.value.value.scoreValue;
          break;
        }        
      }      
    }

    this.dataGridInstance.refresh();
    
  }

  private OpenSnackBar(message, action)
  {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
