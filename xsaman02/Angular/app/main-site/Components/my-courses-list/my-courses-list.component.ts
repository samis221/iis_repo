import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainSiteService } from '../../services/main-site.service';
import { SignUpService } from 'src/app/sign-up.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-my-courses-list',
  templateUrl: './my-courses-list.component.html',
  styleUrls: ['./my-courses-list.component.css']
})
export class MyCoursesListComponent implements OnInit, OnDestroy {

  myCoursesList = null;
  isLoading = true;
  login : String;
  role : String;
  onDestroy$: Subject<Boolean> = new Subject();
  
  constructor(private service : MainSiteService, private cookie : CookieService) { }

  ngOnInit() {
    this.login = this.cookie.get('login');
    this.role = this.cookie.get('role');
    this.myCoursesList = this.service.getMyCoursesList(this.login)
    .pipe(takeUntil(this.onDestroy$))
    .subscribe((data) => {this.myCoursesList = data}, (error) => {console.error(error)});
    
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

}
