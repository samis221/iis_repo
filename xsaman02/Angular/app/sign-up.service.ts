import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Md5 } from 'ts-md5/dist/md5';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignUpService implements OnDestroy {

  onDestroy$: Subject<Boolean> = new Subject();


  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  constructor(private http : HttpClient) { }

  private role = null;

  async Verify(inputCredentials)
  { 
    await this.delay(50);    

    let url = "http://127.0.0.1:12000/sign";
    let login = inputCredentials.value.login;
    let psw = Md5.hashStr(inputCredentials.value.password);

    try {
      let response = await this.http
        .post(url, JSON.stringify({login: login, password: psw}))
        .toPromise();
      return response;
    } catch (error) { console.log(error);}

  }

  private delay(ms: number) : Promise<void> {
    return new Promise<void>(resolve =>
      setTimeout(resolve, ms));
  }
}
