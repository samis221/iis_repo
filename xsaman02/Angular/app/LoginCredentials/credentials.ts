export interface Credentials {
    login : String
    password : String
    role : String
}

export const credentials : Credentials[] = [
    {login : "xadmin00", password : "admin", role : "admin"},
    {login : "xsaman02", password : "a", role : "student"},
    {login : "xstoja07", password : "a", role : "lecturer"},
    {login : "xnevim00", password : "a", role : "guarantor"},
    {login : "xjezek01", password : "a", role : "head"},
]
