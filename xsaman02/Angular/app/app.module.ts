import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { NavigationBarComponent } from "./main-site/Components/navigation-bar/navigation-bar.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ScheduleComponent } from './main-site/Components/schedule/schedule.component';
import { AllCoursesListComponent } from './main-site/Components/all-courses-list/all-courses-list.component';
import { MyCoursesListComponent } from './main-site/Components/my-courses-list/my-courses-list.component';
import { AddNewAccountComponent } from './main-site/Components/add-new-account/add-new-account.component';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatPseudoCheckboxModule, MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule, MatToolbarModule, MatSidenavModule, MatListModule, MatDatepickerModule, MatSnackBarModule } from '@angular/material';

import { MainSiteService } from './main-site/services/main-site.service';
import { ManageAccountsComponent } from './main-site/Components/manage-accounts/manage-accounts.component';

import { DxDataGridModule, DxButtonModule, DxTextBoxModule, DxSelectBoxModule, DxTemplateModule, DxTextAreaModule, DxPivotGridModule, DxTreeListModule, DxDateBoxModule, DxCheckBoxComponent, DxCheckBoxModule, DxPieChartModule, DxSchedulerModule } from 'devextreme-angular';
import { CourseDetailComponent } from './main-site/Components/course-detail/course-detail.component';
import { SignUpService } from './sign-up.service';
import { NewCourseComponent } from './main-site/Components/new-course/new-course.component';
import { ScoreTermComponent } from './main-site/Components/score-term/score-term.component';
import { ApproveCoursesComponent } from './main-site/Components/approve-courses/approve-courses.component';
import { ApproveRegistrationsComponent } from './main-site/Components/approve-registrations/approve-registrations.component';
import { HttpClientModule } from '@angular/common/http';


import { CookieService } from "ngx-cookie-service";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    NavigationBarComponent,
    NewCourseComponent,
    MyCoursesListComponent,
    AllCoursesListComponent,
    AddNewAccountComponent,
    ManageAccountsComponent,
    CourseDetailComponent,
    SignUpComponent,
    ScheduleComponent,
    ScoreTermComponent,
    ApproveCoursesComponent,
    ApproveRegistrationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatCardModule,
    MatInputModule,
    MatPseudoCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatTooltipModule,
    DxDataGridModule,
    DxTemplateModule,
    DxPivotGridModule,
    DxButtonModule,
    DxPieChartModule,
    DxCheckBoxModule,
    DxSchedulerModule,
    DxTemplateModule,
    DxDateBoxModule,
    DxSelectBoxModule,
    MatExpansionModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule
  ],
  providers: [MainSiteService, SignUpService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
