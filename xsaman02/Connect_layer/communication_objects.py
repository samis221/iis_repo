

class Address(dict):
    def __init__(self, country, address, postal_code):
        dict.__init__(self, country = country)
        dict.__init__(self, address = address)
        dict.__init__(self, postalCode = postal_code)

class AccountsDetail(dict):
    def __init__(self,login, first_name, last_name, titles_before, titles_after, numbers, role, address):
        dict.__init__(self, login = login)
        dict.__init__(self, firstName = first_name)
        dict.__init__(self, lastName = last_name)
        dict.__init__(self, titlesBefore = titles_before)
        dict.__init__(self, titlesAfter = titles_after)
        dict.__init__(self, numbers = numbers)
        dict.__init__(self, role = role)
        dict.__init__(self, address = address)

class Account(dict):
        def __init__(self,login, role, marked = False):
            dict.__init__(self, login = login)
            dict.__init__(self, role = role)
            dict.__init__(self, marked = marked)

class DateOfCourse(dict):
    def __init__(self,id, start_date, end_date, current_capacity, room, max_capacity, lecturer):
        dict.__init__(self, id = id)
        dict.__init__(self, copyId = id)
        dict.__init__(self, startDate = start_date)
        dict.__init__(self, endDate = end_date)
        dict.__init__(self, currentCapacity = current_capacity)
        dict.__init__(self, room = room)
        dict.__init__(self, maxCapacity = max_capacity)
        dict.__init__(self, lecturer = lecturer)

class CourseDetail(dict):
    def __init__(self, code, name, description, leader, head, date_of_course):
        dict.__init__(self, code = code)
        dict.__init__(self, name = name)
        dict.__init__(self, description = description)
        dict.__init__(self, leader = leader)
        dict.__init__(self, head = head)
        dict.__init__(self, date_of_course = date_of_course)

class CoursesList(dict):
    def __init__(self, code, name, guarantor):
        dict.__init__(self, code = code)
        dict.__init__(self, name = name)
        dict.__init__(self, guarantor = guarantor)

class MyCoursesList(dict):
     def __init__(self, code, name, startDate, score, signed_as, endDate):
        dict.__init__(self, code = code)
        dict.__init__(self, name = name)
        dict.__init__(self, startDate = startDate)
        dict.__init__(self, score = score)
        dict.__init__(self, signedAs = signed_as)
        dict.__init__(self, endDate = endDate)

class Staff(dict):
    def __init__(self, login, name):
        dict.__init__(self, login = login)
        dict.__init__(self, name = name)

class UnapprovedCourse(dict):
    def __init__(self, code, name, approved):
        dict.__init__(self, code = code)
        dict.__init__(self, name = name)
        dict.__init__(self, approved = approved)

class pendingRegistration(dict):
    def __init__(self, login, code, name, approved):
        dict.__init__(self, login = login)
        dict.__init__(self, code = code)
        dict.__init__(self, name = name)
        dict.__init__(self, approved = approved)

class studentsOnTermBig(dict):
        def __init__(self, max_rating, list):
            dict.__init__(self, maxRating = max_rating)
            dict.__init__(self, students = list)

class studentOnTerm(dict):
    def __init__(self, login, rating):
        dict.__init__(self, login = login)
        dict.__init__(self, rating = rating)
