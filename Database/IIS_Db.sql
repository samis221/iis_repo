-- drop database if exists xstoja07;
create database if not exists iis_db_xstoja07;
use iis_db_xstoja07;


ALTER DATABASE iis_db_xstoja07
    DEFAULT CHARACTER SET utf8mb4
	COLLATE utf8mb4_unicode_520_ci;


-- ---------------adding users -------------------

CREATE USER IF NOT EXISTS 'mid_layer'@'localhost' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON iis_db_xstoja07 . * TO 'mid_layer'@'localhost';

-- ----------------- DROP TABLES ---------------------

drop table if exists Rating;
drop table if exists Registration;
drop table if exists Term;
drop table if exists Room;
drop table if exists Course;
drop table if exists Staff;
drop table if exists Student;
drop table if exists Person;

-- ----------------- CREATE TABLES ---------------------

create table Person (Login varchar(8) PRIMARY KEY NOT NULL,
                    Name varchar(100) NOT NULL,
                    Title_before varchar(100),
                    Title_after varchar(100),
                    email varchar(100) NOT NULL,
                    Phone_number varchar(13),
                    password varchar(100) NOT NULL
) ENGINE=InnoDB;

create table Student (Login varchar(8) NOT NULL,
                    Grade varchar(8) NOT NULL,
                    CONSTRAINT FK_stud_login FOREIGN KEY (Login) REFERENCES Person(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE
) ENGINE=InnoDB;

create table Staff (Login varchar(8) NOT NULL,
                    Role ENUM("lecturer", "guarantor", "head", "admin") NOT NULL,
                    Institute varchar(100),
                    CONSTRAINT FK_staff_login FOREIGN KEY (Login) REFERENCES Person(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE
) ENGINE=InnoDB;

create table Course (ID varchar(3) PRIMARY KEY NOT NULL,
                    Name varchar(100),
                    Description varchar(500),
                    Approved ENUM("Yes", "No", "Pending") NOT NULL,
                    Guarantor varchar(8) NOT NULL,
                    Head varchar(8) NOT NULL,
                    CONSTRAINT FK_course_head FOREIGN KEY (Head) REFERENCES Person(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                    CONSTRAINT FK_course_guarantor FOREIGN KEY (Guarantor) REFERENCES Person(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE
) ENGINE=InnoDB;

create table Room (ID varchar(4) PRIMARY KEY,
                    Location Varchar(100) NOT NULL,
                    Type ENUM("hall", "lab", "CVT") NOT NULL,
                    Capacity integer NOT NULL,
                    Equipment varchar(100)
) ENGINE=InnoDB;

create table Term (Name varchar(100) PRIMARY KEY NOT NULL,
                    Staff_Login varchar(8),
                    Course_ID varchar(3) NOT NULL,
                    Room_ID varchar(4),
                    Rating float NOT NULL,
                    Start_Date datetime NOT NULL,
                    End_Date datetime NOT NULL,
                    CONSTRAINT FK_course_id FOREIGN KEY (Course_ID) REFERENCES Course(ID) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                    CONSTRAINT FK_term_room FOREIGN KEY (Room_ID) REFERENCES Room(ID) 
                        ON UPDATE CASCADE
                        ON DELETE CASCADE
) ENGINE=InnoDB;

create table Registration (Term_name varchar(100),
                            Staff_Login varchar(8),
                            Person_Login varchar(8),
                            Approved ENUM("Yes", "No", "Pending") NOT NULL,
                            CONSTRAINT FK_term_name FOREIGN KEY (Term_name) REFERENCES Term(Name) 
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                            CONSTRAINT FK_reg_person FOREIGN KEY (Person_Login) REFERENCES Person(Login) 
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                            CONSTRAINT FK_reg_staff FOREIGN KEY (Staff_Login) REFERENCES Staff(Login) 
                                ON DELETE CASCADE
                                ON UPDATE CASCADE
) ENGINE=InnoDB;

create table Rating (Points float NOT NULL,
                    Term_name varchar(100) NOT NULL,
                    Person_Login varchar(8) NOT NULL,
                    Staff_Login varchar(8) NOT NULL,
                    CONSTRAINT FK_rating_term FOREIGN KEY (Term_name) REFERENCES Term(Name) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                    CONSTRAINT FK_rating_stud FOREIGN KEY (Person_Login) REFERENCES Person(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,
                    CONSTRAINT FK_rating_staff FOREIGN KEY (Staff_Login) REFERENCES Staff(Login) 
                        ON DELETE CASCADE
                        ON UPDATE CASCADE
) ENGINE=InnoDB;

-- ----------------- DATA SETS ---------------------

insert into Person(login, name, email, Phone_number,password) values ("xstoja07", "Radomír Stojan", "xstoja07vutbr.cz", "000000000", "552bb9c1819f5d2012fd69ba1ad4886f");
insert into Person(login, name, email, Phone_number,password) values ("xsaman02", "Jan Šamánek", "xsaman02@vutbr.cz", "111111111", "b8c20d3ab88e11335a85f841dd1d99a6");
insert into Person(login, name, email, Phone_number,password) values ("xjezek00", "Jan Ježek", "xjezek00@vutbr.cz", "222222222", "c7a65436490b8d92c531b889eae12ff2");
insert into Person(login, name, email, Phone_number,password) values ("xjezek01", "Honza Ježek", "xjezek01@vutbr.cz", "222222222", "bacece7f48d2412aeb58ec6d2927d500");
insert into Person(login, name, email, Phone_number,password) values ("xjezek02", "Janus Ježek", "xjezek01@vutbr.cz", "222222222", "625b4c2318c6ecd8daee74d874e52316");

insert into Person(login, name, Title_before, Title_after, email, Phone_number, password) values ("xkolar00", "Dušan Kolář", "Doc. Dr. Ing.", NULL, "xkolar00@vutbr.cz", "333333333", "7a4dfa54a594ac7df860a73a04db9337");
insert into Person(login, name, Title_before, Title_after, email, Phone_number,password) values ("xbidlo00", "Michal Bidlo", "Ing.", "Ph.D.", "xbidlo00@vutbr.cz", "444444444", "45d90a0ac3f00507f74fd656c987715e");
insert into Person(login, name, Title_before, Title_after, email, Phone_number,password) values ("xmedun00", "Alexander Meduna", "Prof. RNDr.", "CSc.", "xmedun00@vutbr.cz", "555555555", "48860fe84326928caab2bbbaaf72aa32");
insert into Person(login, name, Title_before, Title_after, email, Phone_number,password) values ("xvojna00", "Tomáš Vojnar", "Prof. Ing.", "Ph.D.", "xvojna00@vutbr.cz", "666666666", "966f168436e3f4a67c8d29b3f2eb618a");
insert into Person(login, name, Title_before, Title_after, email, Phone_number,password) values ("xadmin00", "Admin Admin", NULL, NULL, "xadmin00@vutbr.cz", "777777777", "6ec23277701af0ff0556077bf23636b0");
-- select * from Person;
-- SELECT "" as " ";


insert into Student(Login, Grade) values ("xstoja07", "3.");
insert into Student(Login, Grade) values ("xjezek00", "2.");
-- select * from Student;
-- SELECT "" as " ";


insert into Staff(Login, Institute, Role) values ("xkolar00", "UIFS", "Guarantor");
insert into Staff(Login, Institute, Role) values ("xbidlo00", "UITS", "Lecturer");
insert into Staff(Login, Institute, Role) values ("xmedun00", "UIMS", "Head");
insert into Staff(Login, Institute, Role) values ("xvojna00", "UITS", "Head");
insert into Staff(Login, Institute, Role) values ("xadmin00", "ADMIN", "Admin");
-- select * from Staff;
-- SELECT "" as " ";


insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ("IFJ", "Formální jazyky a překladače", "xmedun00", "xmedun00", "IFJ IFJ", "Yes");
insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ("IPP", "Principy OOP", "xmedun00", "xkolar00", "IPP IPP", "Pending");
insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ("IMP", "Vestavné systémy", "xbidlo00", "xbidlo00", "IMP IMP", "No");
insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ("INC", "Číslicové systémy", "xbidlo00", "xbidlo00", "INC INC", "Yes");
insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ("ISA", "Síťové aplikace", "xkolar00", "xkolar00", "ISA ISA", "Pending");
-- select * from Course;
-- SELECT "" as " ";


insert into Room(ID, Location, Type, Capacity, Equipment) values ("L304", "Pavilon L", "Lab", "20", "Lab equip");
insert into Room(ID, Location, Type, Capacity, Equipment) values ("G204", "Pavilon G", "Hall", "300", "Hall equip");
insert into Room(ID, Location, Type, Capacity, Equipment) values ("E104", "Pavilon P", "CVT", "30", "CVT equip");
-- select * from Room;
-- SELECT "" as " ";


insert into Term(Name, Staff_Login, Course_ID, Room_ID, Rating, Start_Date, End_Date) values ("IFJ Term1", "xmedun00", "IFJ", "L304", "100", "2019.8.23 00:00:00", "2019.8.23 01:00:00");
insert into Term(Name, Staff_Login, Course_ID, Room_ID, Rating, Start_Date, End_Date) values ("IFJ Term2", "xmedun00", "IFJ", "G204", "50", "2019.8.23 5:15:00", "2019.8.23 6:15:00");
insert into Term(Name, Staff_Login, Course_ID, Room_ID, Rating, Start_Date, End_Date) values ("IPP Term1", "xbidlo00", "IPP", "G204", "80", "2019.8.5 10:00:38", "2019.8.5 11:00:38");
insert into Term(Name, Staff_Login, Course_ID, Room_ID, Rating, Start_Date, End_Date) values ("IMP Term1", "xbidlo00", "IMP", "E104", "0", "2018.5.15 00:00:01", "2018.5.15 01:00:01");
-- select * from Term;
-- SELECT "" as " ";


insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IFJ Term1", "xmedun00", "xstoja07", "Pending");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IPP Term1", "xbidlo00", "xjezek00", "Yes");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IFJ Term2", "xbidlo00", "xjezek00", "Yes");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IPP Term1", "xkolar00", "xbidlo00", "Pending");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IPP Term1", NULL, "xstoja07", "Pending");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IFJ Term1", "xmedun00", "xbidlo00", "Yes");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IMP Term1", "xkolar00", "xjezek00", "No");
insert into Registration(Term_name, Staff_Login, Person_Login, Approved) values ("IFJ Term1", "xmedun00", "xjezek00", "Yes");
-- select * from Registration;
-- SELECT "" as " ";


insert into Rating(Points, Term_name, Person_Login, Staff_Login) values ("10", "IFJ Term1", "xstoja07", "xmedun00");
insert into Rating(Points, Term_name, Person_Login, Staff_Login) values ("20", "IFJ Term1", "xbidlo00", "xmedun00");
insert into Rating(Points, Term_name, Person_Login, Staff_Login) values ("5.5", "IFJ Term2", "xjezek00", "xbidlo00");
insert into Rating(Points, Term_name, Person_Login, Staff_Login) values ("8.7", "IMP Term1", "xjezek00", "xkolar00");
-- select * from Rating;
-- SELECT "" as " ";

-- ----------------- SELECTS ---------------------

-- SELECT Person.* FROM Student
-- INNER JOIN Person ON Student.Login = Person.Login;
-- SELECT "" as " ";

-- DELETE FROM Person WHERE Login = "xstoja07";

-- SELECT Person.* FROM Student
-- INNER JOIN Person ON Student.Login = Person.Login;
-- select * from Term;
-- SELECT "" as " ";

-- DELETE FROM Course Where ID = "IFJ";
-- select * from Term;
-- SELECT "" as " ";
-- select * from Staff;
-- SELECT "" as " ";
-- select * from Room;
-- SELECT "" as " ";
-- select * from Rating;
-- SELECT "" as " ";
-- select * from Registration;
-- SELECT "" as " ";

-- DELETE FROM Term WHERE Name = "IFJ TERM1";
-- DELETE FROM Room WHERE ID = "L304";
-- select * from Term;
-- SELECT "" as " ";
-- select * from Course;
-- SELECT "" as " ";
-- select * from Room;
-- SELECT "" as " ";

-- UPDATE Person
-- SET Person.Login = "xbidlo00"
-- WHERE Person.Login = "xkolar00";
-- select * from Person;
-- SELECT "" as " ";



-- -- select for midlayer:
-- -- find all terms for "My terms" as student
-- select Course.ID, Course.Name, Term.Start_Date, Rating.Points, Term.End_Date FROM Course 
-- INNER JOIN Term ON Course.ID = Term.Course_ID
-- INNER JOIN Rating ON Term.Name = Rating.Term_name
-- INNER JOIN Registration ON Term.Name = Registration.Term_name
-- WHERE Registration.Person_Login = "xjezek00";
-- SELECT "" as " ";

-- -- should also work for techers signed as "students"
-- select Course.ID, Course.Name, Term.Start_Date, Rating.Points, Term.End_Date FROM Course 
-- INNER JOIN Term ON Course.ID = Term.Course_ID
-- INNER JOIN Rating ON Term.Name = Rating.Term_name
-- INNER JOIN Registration ON Term.Name = Registration.Term_name
-- WHERE Registration.Person_Login = "xbidlo00";
-- SELECT "" as " ";


-- -- find all terms for "My terms" as techer
-- select Course.ID, Course.Name, Term.Start_Date, Term.End_Date FROM Course 
-- INNER JOIN Term ON Course.ID = Term.Course_ID
-- WHERE Term.Staff_Login = "xbidlo00";
-- SELECT "" as " ";


-- -- return all staff logins and roles
-- select Staff.Login, Staff.Role From Staff;
-- SELECT "" as " ";

-- -- return all student logins
-- select Student.Login From Student;
-- SELECT "" as " ";

-- -- return all head logins and names
-- select Staff.Login, Person.Name From Staff
-- INNER JOIN Person ON Staff.Login = Person.Login
-- WHERE Staff.Role = "Head";
-- SELECT "" as " ";

-- -- return all terms, that are "pending"
-- select Course.ID, Course.Name, Course.Approved From Course
-- WHERE Course.Approved = "Pending";
-- SELECT "" as " ";

-- -- count all ocurances of prefix "xlogin"
-- select Max(Person.Login) From Person
-- WHERE Person.Login LIKE 'xjezek%';
-- SELECT "" as " "

-- -- return all pending registrations and their logins, courses and term names
-- Select Person.Login, Course.ID, Term.Name, Registration.Approved From Person
-- INNER JOIN Registration ON Person.Login = Registration.Person_Login
-- INNER JOIN Term ON Registration.Term_name = Term.Name
-- INNER JOIN Course ON Term.Course_ID = Course.ID
-- WHERE Registration.Approved = "Pending";
-- SELECT "" as " ";

-- -- get all student registered on "Term" and their ratings
-- Select Person.Login, Rating.Points From Rating
-- INNER JOIN Person ON Rating.Person_Login = Person.Login
-- INNER JOIN Registration ON Person.Login = Registration.Person_Login
-- WHERE Registration.Term_name = "IFJ Term1" AND Rating.Term_name = "IFJ Term1";
-- SELECT "" as " ";

-- Select * from Course;
-- SELECT "" as " ";
-- Select * from Term;
-- SELECT "" as " ";

-- -- update courses
-- UPDATE Course SET 
-- ID = "TES", Name = "TEST", Head = "xmedun00", Guarantor = "xbidlo00", Description = "test test", Approved = "Pending"
-- WHERE Course.ID = "IMP";

-- Select * from Course;
-- SELECT "" as " ";
-- Select * from Term;
-- SELECT "" as " ";

-- select Course.ID, Term.Name, Term.Start_Date, Rating.Points, Term.End_Date FROM Course
--                                     INNER JOIN Term ON Course.ID = Term.Course_ID
--                                     INNER JOIN Rating ON Term.Name = Rating.Term_name
--                                     WHERE Rating.Person_Login = 'xjezek00'; 
--                                     SELECT "" as " ";

-- select Course.ID, Term.Name,  Term.Start_Date, Term.End_Date FROM Course
--                                     INNER JOIN Term ON Course.ID = Term.Course_ID
--                                     WHERE Term.Staff_Login = 'xstoja07';
--                                     SELECT "" as " ";