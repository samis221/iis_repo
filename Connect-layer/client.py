from socket import *

serverName = "localhost"
serverPort = 1234
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))
message = str.encode("Input:")
clientSocket.sendto(message, (serverName, serverPort))
receivedMessage, serverAdrress = clientSocket.recvfrom(2048)
print(receivedMessage)
clientSocket.close()


