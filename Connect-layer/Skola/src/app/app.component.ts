import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Škola';
  constructor(private _router : Router){}

  ngOnInit()
  {
    
  }

  get signTable()
  {
    return this._router.url == "/SignUp";
  }


}
