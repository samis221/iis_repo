import { Component, OnInit } from '@angular/core';
import { MainSiteService } from '../../services/main-site.service';
import DataSource from 'devextreme/data/data_source';

import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Events } from '../../data/events';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  dataSource: any;
  courses: Array<Events> = [];
  currentDate: Date = new Date();
  credentials;
  onDestroy$: Subject<Boolean> = new Subject();


  constructor(private service: MainSiteService, private cookie : CookieService) {
  }

  ngOnInit()
  {
    let login = this.cookie.get('login');
    this.service.getData(login)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data) => {
        data.forEach( a => {
          this.courses.push({text: a.code, startDate : new Date(a.startDate), endDate : new Date(a.endDate)}) 
        })
        this.dataSource = new DataSource({
          store: this.courses
        });
      }, (error) => {console.error(error);
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  markWeekEnd(cellData) {
    function isWeekEnd(date) {
      var day = date.getDay();
      return day === 0 || day === 6;
    }
    var classObject = {};
    classObject["employee-"] = true;
    classObject['employee-weekend-'] = isWeekEnd(cellData.startDate)
    return classObject;
  }

  markTraining(cellData) {
    var classObject = {
      "day-cell": true
    }

    classObject[ScheduleComponent.getCurrentTraining(cellData.startDate.getDate())] = true;
    return classObject;
  }

  static getCurrentTraining(date) {
    var result = (date) % 3,
      currentTraining = "training-background-" + result;

    return currentTraining;
  }

  get innerWidth()
  {
    return window.innerWidth;
  }

  get innerHeight()
  {
    return window.innerHeight;
  }
}

