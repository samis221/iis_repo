import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit, OnDestroy {

  login : string = null;
  role : string = null;
  lockSidebar = false;

  constructor(private _router: Router, private activeRoute: ActivatedRoute,
    private router: Router, private cookie : CookieService) 
    { 
      this.router.events.subscribe((ev) => 
      {
        if (ev instanceof NavigationEnd) 
        {
          this.login = this.cookie.get("login");
          this.role = this.cookie.get("role");

          if ((!this.login || !this.role) && (this.router.url != "/" && this.router.url != "/SignUp")) {
            alert("Couldn't figure out credentials\nneed to resign.");
            this.cookie.deleteAll();
            this._router.navigate(["/SignUp"]);
          }
          else
          {
            this.cookie.set("login", this.login, 1 / 24);
            this.cookie.set("role", this.role, 1 / 24);      
          }
        }
      });
    }

  ngOnInit() {
    this.login = this.cookie.get("login");
    this.role = this.cookie.get("role");
  }

  ngOnDestroy()
  {
    this.cookie.deleteAll();
  }

  // changeOfRoutes() {
  //   this.login = this.cookie.get("login");
  //   this.role = this.cookie.get("role");

  //   if ((!this.login || !this.role) && (this.router.url != "/" && this.router.url != "/SignUp")) {
  //     alert("Couldn't figure out credentials\nneed to resign.");
  //     this.cookie.deleteAll();
  //     this._router.navigate(["/SignUp"]);
  //   }
  //   else{
  //     this.cookie.set("login", this.login, 1 / 24);
  //     this.cookie.set("role", this.role, 1 / 24);      
  //   }
  // }

  deleteCookies()
  {
    this.cookie.deleteAll();
  }

  get innerWidth() {
    return window.innerWidth;
  }

}
