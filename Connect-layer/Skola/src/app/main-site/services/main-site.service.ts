import { Injectable, OnDestroy } from '@angular/core';
import { roles } from '../data/roles';
import { Observable, Subject } from 'rxjs';
import { countries } from '../data/countries';
import { CoursesDetail } from '../data/courses-detail';
import { MyCoursesList } from '../data/my-courses-list';
import { HttpClient } from "@angular/common/http";
import { FormGroup } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';


@Injectable({
  providedIn: 'root'
})
export class MainSiteService implements OnDestroy {
  
  onDestroy$: Subject<Boolean> = new Subject();

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  constructor(private http : HttpClient) { }

  getRoles() {
    return roles;
  }

  getCountries() {
    return countries;
  }

  async getAccountList() {
    let url = "http://127.0.0.1:12000/accounts_list";
    
    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response;
    } catch (error) {
    }
  }

  getCoursesList() 
  {
    let url = "http://127.0.0.1:12000/courses_list";
    return this.http.get(url);
  }

  async getTermStudents(term)
  {
    console.log(term);
    let url = "http://127.0.0.1:12000/students_on_term/" + term;
    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response;
    } catch (error) {
    }
  }

  getPendingCourses()
  {
    let url = "http://127.0.0.1:12000/unapproved_courses";
    return this.http.get(url);
  }

  async getPendingRegistrations()
  {
    let url = "http://127.0.0.1:12000/pending_registrations";
    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response;
    } catch (error) {
    }
  }

  getMyCoursesList(login: String) {
    let url = "http://127.0.0.1:12000/my_course_list/" + login;
    return this.http.get(url);
  }

  getCourseDetail(code) : Observable<CoursesDetail> {
    let url = "http://127.0.0.1:12000/course_detail/" + code;
    return this.http.get<CoursesDetail>(url);
  }

  async getStaff() {
    let url = "http://127.0.0.1:12000/lecturers";
    // return this.http.get(url);

    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response; 
      ;
    } catch (error) { console.error(error) }
  }

  async getRooms(){
    let url = "http://127.0.0.1:12000/rooms";
    // return this.http.get(url);

    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response;
    } catch (error) {
    }
  }

  getNameByLogin(login) {

    var accounts = this.getStaff();
    
    // return accounts.find(a => {a.login == login});
    return
  }

  async getGuarantors() {
    let url="http://localhost:12000/guarantors";

    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response;
    } catch (error) { console.error(error) }
  }
  async getHeads() {
    let url="http://localhost:12000/heads";
    
    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response; 
      ;
    } catch (error) { console.error(error) }
  }

  async getAccountDetail(login: string) {
    let url = "http://localhost:12000/accounts_detail/" + login;

    try {
      let response = await this.http
        .get(url)
        .toPromise();
      return response; 
    } catch (error) { console.error(error) }
  }

  getData(login) : Observable<MyCoursesList[]>{
    let url = "http://localhost:12000/my_course_list/" + login;
    return <Observable<MyCoursesList[]>> this.http.get(url);
  }

  async addNewCourse(form : FormGroup)
  {
    let url = "http://localhost:12000/add_course";

    try {
      let response = await this.http
        .post(url, JSON.stringify(form.value))
        .toPromise();
      return response;
    } catch (error) {
    }
    // this.http.post(url, JSON.stringify(form.value));
  }

  async addNewAccount(form : FormGroup)
  {
    let url = "http://localhost:12000/add_account";
    form.value.password = Md5.hashStr(form.value.password);
    form.value.confirmPassword = "";
    

    try {
      let response = await this.http
        .post(url, JSON.stringify(form.value))
        .toPromise();
      return response;
    } catch (error) { console.error(error) }
  }

  async scoreAccountOnTerm(obj)
  {
    let url = "http://localhost:12000";
    try {
      let response = await this.http
        .post(url, JSON.stringify(obj))
        .toPromise();
      return response;
    } catch (error) { console.error(error) }
  }

  async deleteAccount(login : string)
  {
    let url = "http://localhost:12000/delete_account/" + login;
    try {
      let response = await this.http
        .delete(url)
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async modifyAccount(account)
  {
    let url = "http://localhost:12000/update_person";
    try {
      let response = await this.http
        .put(url, JSON.stringify(account))
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async modifyPendingCourses(course)
  {
    let url = "http://localhost:12000/update_course/" + course.code;

    try {
      let response = await this.http
        .put(url, JSON.stringify(course))
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async modifyPendingRegistrations(registration, login)
  {
    let url = "http://localhost:12000/update_registration";
    
    try {
      let response = await this.http
        .put(url, JSON.stringify({login: login, registration : registration}))
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async signCourse(body)
  {
    let url = "http://localhost:12000/sign_person_to_term";
    
    try {
      let response = await this.http
        .post(url, JSON.stringify(body))
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async updateCourse(course)
  {
    let url = "http://localhost:12000/update_whole_course";
    
    try {
      let response = await this.http
        .put(url, JSON.stringify(course))
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }

  async deleteCourse(code)
  {
    let url = "http://localhost:12000/delete_course/" + code;
    
    try {
      let response = await this.http
        .delete(url)
        .toPromise();
      return response;
    } catch (error) { console.error(error);}
  }
}
