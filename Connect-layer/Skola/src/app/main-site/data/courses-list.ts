export interface CoursesList {
    code : String;
    name : String,
    guarantor : String,
}

export const coursesList : CoursesList[] = [
    {code : "IDS", name : "databázové systémy", guarantor : "Pavel Růžička"},
    {code : "IPP", name : "objektově orientované programování", guarantor : "Dušan Kolář"},
    {code : "IMS", name : "modelování a simulace", guarantor : "Dr. Pepe"},
    {code : "INM", name : "numerické metody", guarantor : "Dr. Novák"},
    {code : "IZP", name : "základy programování", guarantor : "Petr Havlena"},
    {code : "IDS", name : "databázové systémy", guarantor : "Pavel Růžička"},
    {code : "IPP", name : "objektově orientované programování", guarantor : "Dušan Kolář"},
    {code : "IMS", name : "modelování a simulace", guarantor : "Dr. Pepe"},
    {code : "INM", name : "numerické metody", guarantor : "Dr. Novák"},
    {code : "IZP", name : "základy programování", guarantor : "Petr Havlena"},
    {code : "IDS", name : "databázové systémy", guarantor : "Pavel Růžička"},
    {code : "IPP", name : "objektově orientované programování", guarantor : "Dušan Kolář"},
    {code : "IMS", name : "modelování a simulace", guarantor : "Dr. Pepe"},
    {code : "INM", name : "numerické metody", guarantor : "Dr. Novák"},
    {code : "IZP", name : "základy programování", guarantor : "Petr Havlena"},
    {code : "IDS", name : "databázové systémy", guarantor : "Pavel Růžička"},
    {code : "IPP", name : "objektově orientované programování", guarantor : "Dušan Kolář"},
    {code : "IMS", name : "modelování a simulace", guarantor : "Dr. Pepe"},
    {code : "INM", name : "numerické metody", guarantor : "Dr. Novák"},
    {code : "IZP", name : "základy programování", guarantor : "Petr Havlena"},
    {code : "IDS", name : "databázové systémy", guarantor : "Pavel Růžička"},
    {code : "IPP", name : "objektově orientované programování", guarantor : "Dušan Kolář"},
    {code : "IMS", name : "modelování a simulace", guarantor : "Dr. Pepe"},
    {code : "INM", name : "numerické metody", guarantor : "Dr. Novák"},
    {code : "IZP", name : "základy programování", guarantor : "Petr Havlena"},
]
