export interface Countries {
    id : number,
    name : String
}

export const countries :Countries[] = [
    {id : 0, name: "Czech republic"},
    {id : 1, name: "Germany"},
    {id : 2, name: "Austria"},
    {id : 3, name: "Slovakia"},
    {id : 4, name: "Hungary"}
]