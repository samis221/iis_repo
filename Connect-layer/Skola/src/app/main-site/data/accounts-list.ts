export interface AccountsList {
    login : String;
    role : String;
    marked : Boolean;
}

export const accountsList : AccountsList[] = [
    {login : "xsaman02", role : "Student", marked : false},
    {login : "xstoja07", role : "Student", marked : false},
    {login : "xludvi08", role : "Leader", marked : false},
    {login : "xnevim00", role : "Head", marked : false},
]