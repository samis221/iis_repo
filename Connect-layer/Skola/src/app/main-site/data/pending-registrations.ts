export interface PendingRegistrations {
    login : String
    course : String
    term : Date
    approved : String
}


export const pendingRegistrations : PendingRegistrations[] = [
    {login: "xsaman02", course: "IZP", term: new Date(), approved: "no"},
    {login: "xstoja07", course: "IKR", term: new Date(), approved: "no"},
    {login: "xnevim00", course: "IIS", term: new Date(), approved: "no"},
    {login: "xexamp01", course: "ISS", term: new Date(), approved: "no"},
    {login: "xjezek11", course: "IPK", term: new Date(), approved: "no"},
    {login: "xscavn17", course: "IPP", term: new Date(), approved: "no"}
]