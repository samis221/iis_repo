export interface Roles {
    id : number,
    name : string
}

export const roles : Roles[] = [
    {id : 0, name : "student"},
    {id : 1, name : "lecturer"},
    {id : 2, name : "guarantor"},
    {id : 3, name : "head"}
]
