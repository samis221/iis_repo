export interface DateOfCourse {
    name : string,
    startDate : Date,
    endDate : Date,
    currentCapacity : number,
    maxCapacity : number,
    lector : String
}
