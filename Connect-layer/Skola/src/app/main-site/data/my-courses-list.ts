

export interface MyCoursesList {
    code : String;
    name : String;
    startDate : Date;
    endDate : Date;
    signedAs : String;
    score : number;
}
