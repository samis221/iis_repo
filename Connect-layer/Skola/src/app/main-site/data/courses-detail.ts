import { DateOfCourse } from './date-of-course';

export interface CoursesDetail {
    code : String,
    name : String,
    description : String,
    leader : String,
    head : String,
    dates : DateOfCourse[]
}

export const coursesDetail : CoursesDetail[] = [
    {code : "IDS", name : "databázové systémy", description : "předmět se zabývá relačními databázemi, jejich implementací a využitím",
     leader : "xruzic11", head : "xexamp00", 
        dates : [
            {name : "0", startDate: new Date("2020-09-19T10:00"), endDate : new Date("2020-09-19T14:00"), currentCapacity : 11, maxCapacity : 12, lector : "xhavle00"},
            {name : "1", startDate: new Date("2020-09-26T12:00"), endDate : new Date("2020-09-26T15:00"), currentCapacity : 26, maxCapacity : 26, lector : "xruzic11"},
            {name : "2", startDate: new Date("2020-10-01T08:00"), endDate : new Date("2020-10-01T10:45"), currentCapacity : 0, maxCapacity : 12, lector : "xhavle00"}
        ]
    },
    {code : "IZP", name : "základy programování", description : "Nauka o základech modulárního programování a jazyce C", 
     leader : "xhavle00",  head : "xexamp00",
        dates : [{
            name : "3", startDate: new Date("2020-03-01T15:35"), endDate : new Date("2020-03-01T17:00"), currentCapacity : 332, maxCapacity : 545, lector : "xhavle00" 
        }]
    },
    {code : "IPP", name : "objektově orientované programování", description : "Velice dlouhý popisek předmětu který ověří jak se bude text zalamovat a jaký to bude mít účinek na celou stránku/ Celý Text je napsaný ručně a proto si vážím veškerých symptaií k mé straně/ Bohužel mi dochází nápady a nevím co dál psát a proto to nejspíš za chvíli utnu/ Ještě byl ale potřeboval text trošku prodloužit aby byl test zalamování textu kvalitní/ Myslím že tohle už by mohlo stačit takže tohle je má poslední věta/",
     leader : "xkolar00",  head : "xexamp00",
        dates : [
            { name : "4", startDate: new Date("2019-10-04T10:15"), endDate : new Date("2019-10T04:11"), currentCapacity : 5, maxCapacity : 199, lector : "xkolar00"},
            { name : "5", startDate: new Date("2019-10-13T12:15"), endDate : new Date("2019-10T13:13"), currentCapacity : 55, maxCapacity : 199, lector : "xkolar00"},  
        ]
    },
    {code : "IMS", name : "modelování a simulace", description : "zatím neznám", 
     leader : "xperin00",  head : "xexamp00",
        dates : [{
            name : "6", startDate: new Date("2019-11-15T18:00"), endDate : new Date("2019-11-15T20:00"), currentCapacity : 354, maxCapacity : 600, lector : "xruzic11" 
        }]
    },
    {code : "INM", name : "numerické metody", description : "nauka o početních metodách řešení matematických úloh",
     leader : "xnovak00",  head : "xexamp00",
        dates : [
            { name : "7", startDate: new Date("2019-10-29T11:00"), endDate : new Date("2019-10-29T12:00"), currentCapacity : 488, maxCapacity : 500, lector : "xperin00"},
            { name : "8", startDate: new Date("2019-11-06T10:00"), endDate : new Date("2019-11-06T14:35"), currentCapacity : 500, maxCapacity : 500, lector : "xnovak00"}
        ]
    }
]
