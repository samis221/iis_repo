import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { AllCoursesListComponent } from "./Components/all-courses-list/all-courses-list.component";
// import { MyCoursesListComponent } from "./Components/my-courses-list/my-courses-list.component";
// import { NavigationBarComponent } from "./Components/navigation-bar/navigation-bar.component";
// import { ScheduleComponent } from "./Components/schedule/schedule.component";
// import { AddNewAccountComponent } from './Components/add-new-account/add-new-account.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
// import { ManageAccountsComponent } from './Components/manage-accounts/manage-accounts.component';

import { DxDataGridModule, DxButtonModule, DxSelectBoxModule, DxTemplateModule, DxPivotGridModule, DxDateBoxModule, DxCheckBoxModule, DxPieChartModule, DxSchedulerModule } from 'devextreme-angular';
// import { CourseDetailComponent } from './Components/course-detail/course-detail.component';
import { MatExpansionModule, MatToolbarModule, MatSidenavModule, MatListModule, MatDatepickerModule, MatNativeDateModule, MatSnackBarModule } from '@angular/material';
// import { NewCourseComponent } from './Components/new-course/new-course.component';
import { Routes, RouterModule } from '@angular/router';
// import { ScoreTermComponent } from './Components/score-term/score-term.component';
// import { ApproveCoursesComponent } from './Components/approve-courses/approve-courses.component';
// import { ApproveRegistrationsComponent } from './Components/approve-registrations/approve-registrations.component';

const routes: Routes = [
//   { path: ":login/Schedule", component : ScheduleComponent},
//   { path: ":login/MyCourses", component : MyCoursesListComponent},
//   { path: ":login/AllCourses", component : AllCoursesListComponent},
//   { path: ":login/AllCourses/:code", component : CourseDetailComponent},
];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    DxDataGridModule,
    DxTemplateModule,
    DxPivotGridModule,
    DxButtonModule,
    DxDateBoxModule,
    DxPieChartModule,
    DxCheckBoxModule,
    DxSchedulerModule,
    DxSelectBoxModule,
    DxTemplateModule,
    MatExpansionModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule

  ]
})
export class MainSiteModule { }
