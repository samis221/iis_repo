import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SignUpService } from '../sign-up.service';
import { Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

export interface Todo
{
  userId: Number,
  id: Number,
  title: String,
  completed: Boolean
}


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit, OnDestroy {

  signForm : FormGroup;
  wrongCredentials : Boolean;
  onDestroy$: Subject<Boolean> = new Subject();

  // placeholders;
  constructor(private fb : FormBuilder, private _router : Router, 
              private service : SignUpService, private cookie : CookieService) { }

  ngOnInit() {
    this.CreateForm();
    // this.placeholders = this.service.GetPlaceholders().subscribe((data) => {console.log(data); this.placeholders = data})
    
  }

  ngOnDestroy(): void {
    this.onDestroy$.next(true);
  }

  private CreateForm()
  {
    this.signForm = this.fb.group({
      login : this.fb.control('', [Validators.required, Validators.pattern("x\\S{5}\\d{2}")]),
      password : this.fb.control('', [Validators.required])
    });
  }

  async ValidateCredentials()
  {
    var acc;
    this.wrongCredentials = false;

    acc = await this.service.Verify(this.signForm);
    await this.delay(500);

    if (acc) {
      this._router.navigate(['/', 'Schedule']);
      this.cookie.set("login", this.signForm.value.login, 1 / 24); // 1 hour
      this.cookie.set("role", acc.role, 1 / 24); // 1 hour
    }
    else
    {
      this.wrongCredentials = true;
    }
  }

  SignAsHost()
  {
    this._router.navigate(['/','Schedule'], { queryParams : { login : 'host', role : 'host'}});
    this.cookie.set("login", "host", 1 / 24);
    this.cookie.set("role", "host", 1 / 24);
  }

  get password()
  {
    return this.signForm.get('password');
  }

  get login()
  {
    return this.signForm.get('login');
  }

  private delay(ms: number) : Promise<void> {
    return new Promise<void>(resolve =>
      setTimeout(resolve, ms));
  }

}

