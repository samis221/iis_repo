import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ScheduleComponent } from './main-site/Components/schedule/schedule.component';
import { AllCoursesListComponent } from './main-site/Components/all-courses-list/all-courses-list.component';
import { MyCoursesListComponent } from './main-site/Components/my-courses-list/my-courses-list.component';
import { AddNewAccountComponent } from './main-site/Components/add-new-account/add-new-account.component';
import { ManageAccountsComponent } from './main-site/Components/manage-accounts/manage-accounts.component';
import { CourseDetailComponent } from './main-site/Components/course-detail/course-detail.component';
import { NewCourseComponent } from './main-site/Components/new-course/new-course.component';
import { ScoreTermComponent } from './main-site/Components/score-term/score-term.component';
import { ApproveCoursesComponent } from './main-site/Components/approve-courses/approve-courses.component';
import { ApproveRegistrationsComponent } from './main-site/Components/approve-registrations/approve-registrations.component';



const routes: Routes = [
  { path: 'Schedule', component: ScheduleComponent},
  { path: "MyTerms", component : MyCoursesListComponent},
  { path: "AllCourses", component : AllCoursesListComponent},
  { path: "ManageAccounts", component : ManageAccountsComponent},
  { path: "AddNewAccount", component : AddNewAccountComponent},
  { path: "NewCourse", component : NewCourseComponent},
  { path: "AllCourses/:code", component : CourseDetailComponent},
  { path: "ScoreTerm/:term", component : ScoreTermComponent},  
  { path: 'SignUp', component: SignUpComponent},
  { path: "ApproveCourses", component : ApproveCoursesComponent},  
  { path: "ApproveRegistrations", component: ApproveRegistrationsComponent},
  { path: '**', component: PageNotFoundComponent }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
