#!usr/bin/env python3

from mid_layer import app

if __name__ == "__main__":
    app.run()