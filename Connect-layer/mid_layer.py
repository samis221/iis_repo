#!usr/bin/env python3

from os import urandom
import json.encoder as json
from datetime import datetime
from database import *
from flask import Flask, request, jsonify, abort
from flask_restful import Resource, Api
from flask_cors import CORS
import json


server_port = 12000
db = DB()
app = Flask(__name__)
CORS(app)
api = Api(app)
app.secret_key = urandom(32).hex()


class Accounts_detail(Resource):
    def get(self, login):
        db_res = db.get_account(login)
        return jsonify(db_res)

class Accounts_list(Resource):
    def get(self):
        db_res = db.get_account_list()
        return jsonify(db_res)

class Course_detail(Resource):
    def get(self, course_code):
        db_res = db.get_course_detail(course_code)
        return jsonify(db_res)

class Courses_list(Resource):
    def get(self):
        db_res = db.get_course_list()
        return jsonify(db_res)

class My_course_list(Resource):
    def get(self, login):
        db_res = db.get_my_course_list(login)
        return jsonify(db_res)

class Head_list(Resource):
    def get(self):
        db_res = db.get_head_list()
        return jsonify(db_res)

class Guarantor_list(Resource):
    def get(self):
        db_res = db.get_guarantor_list()
        return jsonify(db_res)

class Lecturer_list(Resource):
    def get(self):
        db_res = db.get_lecturer_list()
        return jsonify(db_res)

class Room_list(Resource):
    def get(self):
        db_res = db.get_room_list()
        return jsonify(db_res)

class Unapproved_course_list(Resource):
    def get(self):
        db_res = db.get_unapproved_course_list()
        return jsonify(db_res)

class Pending_registrations(Resource):
    def get(self):
        db_res = db.get_pending_registrations()
        return jsonify(db_res)

class Students_on_term(Resource):
    def get(self, term_name):
        db_res = db.get_students_on_term(term_name)
        return jsonify(db_res)


#POST
class Sign(Resource):
     def post(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        result = db.get_logged_user(res['login'], res['password'])
        if result == None:
            return None
        else:
            db_res = db.get_account(res['login'])
            return jsonify(db_res)


class Add_course(Resource):
    def post(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        existing_code = db.test_course(res['code'])
        if existing_code != None:
            abort(400, 'Course already exists')
        else:
            print(res)
            db.insert_course(res['code'], res['name'], res['head'], res['guarantor'], res['description'], 'Pending')
            for x in res['terms']:
                startDate = datetime.strptime(x['startDate'], "%Y-%m-%dT%H:%M:%S.%fZ")
                endDate = datetime.strptime(x['endDate'], "%Y-%m-%dT%H:%M:%S.%fZ")
                startDate = startDate.strftime("%Y.%m.%d %H:%M:%S")
                endDate = endDate.strftime("%Y.%m.%d %H:%M:%S")

                db.insert_term(x['name'], x['lecturer'], res['code'], x['room'], x['maxScore'], startDate, endDate)
            db_res = db.get_course_detail(res['code'])
            return jsonify(db_res)

class Add_account(Resource):
    def post(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        xlogWithoutNumber = 'x' + res['lastName'][0:5]
        xlogWithoutNumber.lower()
        xlogWithoutNumber = xlogWithoutNumber.rjust(6,'x')
        print(xlogWithoutNumber)
        maxIdNumber = db.get_max_login(xlogWithoutNumber)
        maxLogin = maxIdNumber[0]
        if maxLogin == None:
            maxIdNumber = -1
        else:
            maxIdNumber = int(maxLogin[-2:])

        maxIdNumber += 1
        login = xlogWithoutNumber + '{:02d}'.format((maxIdNumber))
        if res['role'] == "student":
            db.insert_student(res['specialization']['yearOfStudy'], login, res['firstName'] + " " + res['lastName'], res['email'], res['password'], res['titlesBefore'], res['titlesAfter'], res['number'])
        else:
            db.insert_staff(res['role'], login, res['firstName'] + " " + res['lastName'], res['email'], res['password'], res['titlesBefore'], res['titlesAfter'], res['number'], res['specialization']['institute'])

        db_res = db.get_account(login)
        return jsonify(db_res)


class Sign_person_to_term(Resource):
     def post(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        max_capacity =res['capacity']
        existing_reg = db.registration_exists(res['id'], res['login'])
        if existing_reg != None:
            abort(400, "Registration already exists")
        else:
            occupancy = db.term_occupancy(res['id'])
            if int(occupancy) < int(max_capacity):
                db.create_registration(res['id'], res['login'])
                return "OK"
            else:
                abort(400, 'Capacity is full')



#DELETE
class Delete_account(Resource):
    def delete(self, login):
        db.delete_account(login)
        return jsonify("Person with this login is not in the database anymore")

class Delete_course(Resource):
    def delete(self, course_id):
        db.delete_course(course_id)
        return jsonify("Course with this id is not in the database anymore")


#PUT
class Update_course(Resource):
    def put(self, course_code):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        if(res['approved'] == 'No'):
            db.delete_course(res['code'])
            return jsonify("Course deleted")
        else:
            db.update_course(course_code, res['code'], res['name'], res['approved'])
            return jsonify("Course modified")


class Update_whole_course(Resource):
    def put(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        if db.test_course(res['oldCode']) == None:
            abort(400, "Course you want update does not exists")
        old_course_code = res['oldCode']
        new_course_code = res['course']['code']
        new_course_name = res['course']['name']
        new_course_head = res['course']['head']
        new_course_guarantor = res['course']['guarantor']
        new_course_description = res['course']['description']
        db.update_whole_course(old_course_code, new_course_code, new_course_name, new_course_head, new_course_guarantor, new_course_description)
        for term in res['terms']:
            old_term_id = term['oldId']
            new_term_id = term['term']['id']
            new_term_start_date = term['term']['startDate']
            new_term_end_date = term['term']['endDate']
            new_term_lecturer = term['term']['lecturer']
            new_term_room = term['term']['room']

            input_format = "%Y-%m-%dT%H:%M:%S.%fZ"
            input_format2 = "%Y/%m/%d %H:%M:%S"
            db_format = "%Y.%m.%d %H:%M:%S"
            format_list = (input_format, input_format2)

            def try_parse_date(date, format_list):
                for format in format_list:
                    try:
                        return datetime.strptime(date, format)
                    except:
                        pass
                abort(400, "Date format:'" + date + "' is invalid")

            new_term_start_date = try_parse_date(new_term_start_date, format_list)
            new_term_end_date = try_parse_date(new_term_end_date, format_list)
            new_term_start_date = new_term_start_date.strftime( db_format)
            new_term_end_date = new_term_end_date.strftime(db_format)

            if db.test_term(old_term_id) == None:
                db.insert_term(new_term_id, new_term_lecturer, new_course_code, new_term_room, '0', new_term_start_date, new_term_end_date)
            else:
                db.update_term(old_term_id, new_term_id, new_term_start_date, new_term_end_date, new_term_lecturer, new_term_room)
        return "OK"

class Update_registration(Resource):
    def put(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        staff = res['login']
        login = res['registration']['login']
        term_id = res['registration']['name']
        approved = res['registration']['approved']
        existing_course = db.registration_exists(term_id, login)
        if(existing_course == None):
            abort(400, 'Updating unexisting registration')
        else:
            if(approved == 'No'):
                db.delete_registration(term_id, login)
                return jsonify("Registration deleted")
            else:
                if(approved == 'Yes'):
                    db.create_rating('0', term_id, login, staff)
                db.update_registration(term_id, login, term_id, staff, login, approved)
                return jsonify("Registration modified")


class Update_person(Resource):
    def put(self):
        res = json.loads(request.data.decode('utf8').replace("'", '"'))
        login = res['login']
        name = res['firstName'] + ' ' + res['lastName']
        titilesBefore = res['titlesBefore']
        titlesAfter = res['titlesAfter']
        number = res['number']
        email = res['email']
        existing_person = db.person_exists(login)
        if(existing_person == None):
            abort(400, 'Updating unexisting person')
        else:
            db.update_person(login, name, titilesBefore, titlesAfter, number, email)
            return jsonify("Registration modified")


# URL POTREBNE PRO PROJEKT
#GET
api.add_resource(Accounts_detail, '/accounts_detail/<login>')
api.add_resource(Accounts_list, '/accounts_list')
api.add_resource(Course_detail, '/course_detail/<course_code>')
api.add_resource(Courses_list, '/courses_list')
api.add_resource(My_course_list, '/my_course_list/<login>')
api.add_resource(Head_list, '/heads')
api.add_resource(Guarantor_list, '/guarantors')
api.add_resource(Room_list, '/rooms')
api.add_resource(Lecturer_list, '/lecturers')
api.add_resource(Unapproved_course_list, '/unapproved_courses')
api.add_resource(Pending_registrations, '/pending_registrations')
api.add_resource(Students_on_term, '/students_on_term/<term_name>')

#POST
api.add_resource(Sign, '/sign')
api.add_resource(Add_course, '/add_course')
api.add_resource(Add_account, '/add_account')
api.add_resource(Sign_person_to_term, '/sign_person_to_term')

#DELETE
api.add_resource(Delete_account, '/delete_account/<login>')
api.add_resource(Delete_course, '/delete_course/<course_id>')

#PUT
api.add_resource(Update_course, '/update_course/<course_code>') # zmena approved
api.add_resource(Update_whole_course, '/update_whole_course')#zmena course a terminu
api.add_resource(Update_registration, '/update_registration')
api.add_resource(Update_person, '/update_person')



if __name__ == '__main__':
     app.run(port=server_port)
