#!usr/bin/env python3

import mysql.connector
from communication_objects import *


class DB:
    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="mid_layer",
            passwd="123456",
            database="iis_db_xstoja07"
        )
        self.mycursor = self.mydb.cursor(buffered=True)

    def get_account(self, login):
        self.mycursor.execute("SELECT login, name, Title_Before, Title_After, email, Phone_number FROM Person WHERE Login = '%s'" % str(login))
        row = self.mycursor.fetchone()
        splitName = row[1].split(" ", 1)

        self.mycursor.execute("SELECT Role FROM Staff WHERE Login = '%s'" % str(login))
        row2 = self.mycursor.fetchone()
        if row2:
            role = row2[0]
        else:
            role = "Student"

        return AccountsDetail(row[0], splitName[0], splitName[1], row[2], row[3], row[5], role, row[4])

    def get_account_list(self):
        self.mycursor.execute('select Staff.Login, Staff.Role From Staff;')
        list = []
        for row in self.mycursor.fetchall():
            list.append(Account(row[0], row[1]))

        self.mycursor.nextset()
        self.mycursor.execute('select Student.Login From Student;')
        for row in self.mycursor.fetchall():
            list.append(Account(row[0], 'student'))

        self.mycursor.nextset()
        return list


    def get_course_detail(self, course_code):
        self.mycursor.execute("SELECT * FROM Course WHERE ID = '%s'" % str(course_code))
        course_row = self.mycursor.fetchone()
        if (course_row == None):
            return None
        self.mycursor.execute("SELECT * FROM Term WHERE Course_ID = '%s'" % str(course_code))

        room_cursor = self.mydb.cursor(buffered=True)
        registration_cursor = self.mydb.cursor(buffered=True)
        term_list = []
        for term_row in self.mycursor.fetchall():
            room_cursor.execute("SELECT * FROM Room WHERE ID = '%s'" % str(term_row[3]))
            room_row = room_cursor.fetchone()
            registration_cursor.execute(
                "SELECT COUNT(*) as 'capacity' FROM Registration WHERE Term_name = '%s' AND Approved = 'Yes'" % str(
                    term_row[0]))
            registration_row = registration_cursor.fetchone()
            term = DateOfCourse(term_row[0], term_row[5], term_row[6], registration_row[0], room_row[0], room_row[3], term_row[1])
            term_list.append(term)

        return CourseDetail(course_row[0], course_row[1], course_row[2], course_row[4], course_row[5], term_list)

    def get_course_list(self):
        self.mycursor.execute("SELECT * FROM Course WHERE Course.Approved = 'yes' ")
        return [CoursesList(row[0], row[1], row[4]) for row in self.mycursor.fetchall()]

    def get_my_course_list(self, login):
        self.mycursor.execute("""select Course.ID, Term.Name, Term.Start_Date, Rating.Points, Term.End_Date FROM Course
                                    INNER JOIN Term ON Course.ID = Term.Course_ID
                                    INNER JOIN Rating ON Term.Name = Rating.Term_name
                                    WHERE Rating.Person_Login = '%s'; 
                                """ % str(login))
        student_list = [MyCoursesList(row[0], row[1], row[2], row[3], 'student', row[4]) for row in
                        self.mycursor.fetchall()]
        self.mycursor.nextset()

        self.mycursor.execute("""select Course.ID, Term.Name,  Term.Start_Date, Term.End_Date FROM Course
                                    INNER JOIN Term ON Course.ID = Term.Course_ID
                                    WHERE Term.Staff_Login = '%s';
                                 """ % str(login))
        lector_list = [MyCoursesList(row[0], row[1], row[2], None, 'lector', row[3]) for row in
                       self.mycursor.fetchall()]
        self.mycursor.nextset()

        return (student_list + lector_list)

    def get_head_list(self):
        self.mycursor.execute("""select Staff.Login, Person.Name FROM Staff
                                    INNER JOIN Person ON Staff.Login = Person.Login
                                    WHERE Staff.Role = "Head" OR Staff.Role = "Admin";""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(Staff(row[0], row[1]))
        self.mycursor.nextset()
        return list


    def get_guarantor_list(self):
        self.mycursor.execute("""select Staff.Login, Person.Name FROM Staff
                                    INNER JOIN Person ON Staff.Login = Person.Login
                                    WHERE Staff.Role = "Guarantor" OR Staff.Role = "Head" OR Staff.Role = "Admin";""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(Staff(row[0], row[1]))
        self.mycursor.nextset()
        return list


    def get_lecturer_list(self):
        self.mycursor.execute("""select Staff.Login, Person.Name FROM Staff
                                    INNER JOIN Person ON Staff.Login = Person.Login;""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(Staff(row[0], row[1]))
        self.mycursor.nextset()
        return list


    def get_room_list(self):
        self.mycursor.execute("""select Room.ID FROM Room;""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(row[0])
        self.mycursor.nextset()
        return list


    def get_unapproved_course_list(self):
        self.mycursor.execute("""select Course.ID, Course.Name, Course.Approved From Course
                                    WHERE Course.Approved = "Pending";""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(UnapprovedCourse(row[0], row[1], row[2]))
        self.mycursor.nextset()
        return list

    def get_logged_user(self, login, password):
        self.mycursor.execute(
            """select Person.login FROM Person WHERE password = '%s' AND login = '%s';""" % (str(password), str(login)))
        result = self.mycursor.fetchone()
        return result

    def get_max_login(self, xloginWithoutNumber):
        self.mycursor.execute("select Max(Person.Login) From Person WHERE Person.Login LIKE " + "'{}%'".format(str(xloginWithoutNumber)))
        return self.mycursor.fetchone()

    def get_pending_registrations(self):
        self.mycursor.execute("""Select Person.Login, Course.ID, Term.Name, Registration.Approved From Person
                                    INNER JOIN Registration ON Person.Login = Registration.Person_Login
                                    INNER JOIN Term ON Registration.Term_name = Term.Name
                                    INNER JOIN Course ON Term.Course_ID = Course.ID
                                    WHERE Registration.Approved = "Pending";""")
        list = []
        for row in self.mycursor.fetchall():
            list.append(pendingRegistration(row[0], row[1], row[2], row[3]))
        self.mycursor.nextset()
        return list

    def get_students_on_term(self, term_name):
                self.mycursor.execute(""" SELECT Term.Rating From Term WHERE Term.Name = '%s' """ % (str(term_name)))
                res = self.mycursor.fetchone()
                term_max_rate = res[0]
                self.mycursor.nextset()

                self.mycursor.execute(""" Select Person.Login, Rating.Points From Rating
                                            INNER JOIN Person ON Rating.Person_Login = Person.Login
                                            INNER JOIN Registration ON Person.Login = Registration.Person_Login
                                            WHERE Registration.Term_name = '%s' AND Rating.Term_name = '%s';""" % (str(term_name),str(term_name)))
                list = []
                for row in self.mycursor.fetchall():
                    list.append(studentOnTerm(row[0], row[1]))
                self.mycursor.nextset()
                return studentsOnTermBig(term_max_rate, list)


#POST
    def insert_course(self,code, name, head, guarantor, description, approved):
        self.mycursor.execute(
            """insert into Course(ID, Name, Head, Guarantor, Description, Approved) values ('%s', '%s', '%s', '%s', '%s', '%s')
            ;""" % (str(code), str(name), str(head), str(guarantor), str(description), str(approved)))
        self.mydb.commit()

    def insert_term(self, name, staff_login, course_id, room_id, rating, start_date, end_date):
        self.mycursor.execute(
            """insert into Term(Name, Staff_Login, Course_ID, Room_ID, Rating, Start_Date, End_Date) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')
            ;""" % (str(name), str(staff_login), str(course_id), str(room_id), str(rating), str(start_date), str(end_date)))
        self.mydb.commit()

    def insert_person(self, login, name, email, password, title_before = None, title_after = None, phone_number = None):
        self.mycursor.execute(
            """insert into Person(login, name, Title_before, Title_after, email, Phone_number, password) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')
            ;""" % (str(login), str(name), str(title_before), str(title_after), str(email), str(phone_number), str(password)))
        self.mydb.commit()

    def insert_student(self, grade, login, name, email, password, title_before = None, title_after = None, phone_number = None):
        self.insert_person(login, name, email, password, title_before, title_after, phone_number)
        self.mycursor.execute("""insert into Student(Login, Grade) values ('%s', '%s');""" % (str(login), str(grade),))
        self.mydb.commit()

    def insert_staff(self, role, login, name, email, password, title_before = None, title_after = None, phone_number = None, institute = None):
        self.insert_person(login, name, email, password, title_before, title_after, phone_number)
        self.mycursor.execute("""insert into Staff(Login, Institute, Role) values ('%s', '%s', '%s');""" % (str(login), str(institute), str(role),))
        self.mydb.commit()

    def create_registration(self,id, login):
        self.mycursor.execute("""insert into Registration(Term_name, Person_Login, Approved, Staff_Login) values ('%s', '%s', 'Pending', NULL)
          ;""" % (str(id), str(login),))
        self.mydb.commit()


#DELETE
    def delete_account(self, login):
        self.mycursor.execute("""DELETE FROM Person WHERE Login = '%s';""" % str(login))
        self.mydb.commit()

    def delete_course(self, course_code):
        self.mycursor.execute("""DELETE FROM Course WHERE ID = '%s';""" % str(course_code))
        self.mydb.commit()

#PUT
    def update_course(self,old_course_code, new_course_code, name, approved):
        self.mycursor.execute("""UPDATE Course SET ID = '%s', Name = '%s', Approved = '%s' WHERE ID = '%s';""" % (str(new_course_code),str(name),str(approved),str(old_course_code)))
        self.mydb.commit()

    def update_whole_course(self,old_course_code, new_course_code, name, head, guarantor, description):
        self.mycursor.execute("""UPDATE Course SET ID = '%s', Name = '%s', Head = '%s', Guarantor = '%s', Description = '%s'
            WHERE Course.ID = '%s';""" % (str(new_course_code),str(name),str(head),str(guarantor),str(description),str(old_course_code)))
        self.mydb.commit()

    def update_term(self,old_term_id, new_term_id, startDate, endDate, lecturer, room):
        self.mycursor.execute("""UPDATE Term SET Name = '%s', Staff_Login = '%s', Room_ID = '%s',  Start_Date = '%s',  End_Date = '%s' WHERE Name = '%s'
        ;""" % (str(new_term_id),str(lecturer),str(room),str(startDate),str(endDate),str(old_term_id)))
        self.mydb.commit()

    def update_registration(self, old_term_id, old_login, term_id, staff, login, approved):
        self.mycursor.execute("""UPDATE Registration SET Term_Name = '%s', Staff_Login = '%s', Person_Login = '%s', Approved = '%s' WHERE Term_name = '%s' AND Person_Login = '%s'
        ;""" % (str(term_id), str(staff), str(login), str(approved), str(old_term_id), str(old_login)))
        self.mydb.commit()

    # cant update login
    def update_person(self,login, name, titlesBefore, titlesAfter, number, email):
        self.mycursor.execute("""UPDATE Person SET Person.name = '%s', Person.Title_before = '%s', Title_after = '%s', email = '%s' , Phone_number = '%s'WHERE Person.login = '%s'
        ;""" % (str(name), str(titlesBefore), str(titlesAfter), str(email), str(number), str(login)))
        self.mydb.commit()

    def create_rating(self, points, term_name, person_login, staff_login):
        self.mycursor.execute("""insert into Rating(Points, Term_name, Person_Login, Staff_Login) values ('%s', '%s', '%s', '%s')
                                    ;""" % (str(points), str(term_name), str(person_login), str(staff_login),))
        self.mydb.commit()


#SUPPORT FUNCTIONS
    def test_course(self, course_code):
        self.mycursor.execute("SELECT Course.ID FROM Course WHERE ID = '%s'" % str(course_code))
        course_row = self.mycursor.fetchone()
        return course_row

    def test_term(self, term_name):
        self.mycursor.execute("SELECT Term.Name FROM Term WHERE Name = '%s'" % str(term_name))
        course_row = self.mycursor.fetchone()
        return course_row

    def term_occupancy(self, term_id):
        self.mycursor.execute("SELECT COUNT(*) as 'capacity' FROM Registration WHERE Term_name = '%s' AND ( Approved = 'Yes' OR Approved = 'Pending');" % str(term_id))
        res = self.mycursor.fetchone()
        return res[0]

    def registration_exists(self, term_id, login):
        self.mycursor.execute("Select * From Registration WHERE Registration.Term_name = '%s' AND Registration.Person_Login = '%s';" % (str(term_id), str(login)))
        res = self.mycursor.fetchone()
        return res

    def delete_registration(self, term_id, login):
        self.mycursor.execute("""DELETE FROM Registration WHERE Term_Name = '%s' AND Person_Login = '%s';""" % (str(term_id), str(login)))
        self.mydb.commit()

    def person_exists(self, login):
        self.mycursor.execute("Select * From Person WHERE Person.login = '%s';" % (str(login)))
        res = self.mycursor.fetchone()
        return res


